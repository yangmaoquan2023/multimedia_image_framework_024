# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/ohos/ace/ace.gni")
import("//commonlibrary/memory_utils/purgeable_mem_config.gni")
import("//foundation/multimedia/image_framework/ide/image_decode_config.gni")
import("$image_subsystem/plugins/cross/image_native_android.gni")
import("$image_subsystem/plugins/cross/image_native_ios.gni")

config("image_external_config") {
  include_dirs = [
    "include",
    "//utils/system/safwk/native/include",
    "//foundation/ability/ability_runtime/interfaces/inner_api/runtime/include/",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/include",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/include",
    "//foundation/multimedia/image_framework/frameworks/kits/js/common/include",
    "//foundation/multimedia/image_framework/interfaces/kits/js/common/include",
    "//foundation/multimedia/utils/include",
    "//foundation/multimedia/image_framework/plugins/common/libs/image/libjpegplugin",
    "//foundation/multimedia/image_framework/plugins/manager/include",
    "//foundation/multimedia/image_framework/plugins/manager/include/image",
    "//foundation/multimedia/image_framework/interfaces/innerkits/include",
    "//foundation/multimedia/image_framework/interfaces/kits/native/include",
    "//utils/jni/jnikit/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "$graphic_surface_root/interfaces/inner_api/surface",
    "//foundation/graphic/graphic_2d/interfaces/inner_api/common",
    "//foundation/graphic/graphic_2d/interfaces/kits/napi/graphic/color_manager/color_space_object_convertor",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
    "//foundation/graphic/graphic_2d/utils/buffer_handle/export",
    "//foundation/graphic/graphic_2d/utils/color_manager/export",
    "//drivers/peripheral/display/interfaces/include",
    "//drivers/peripheral/base",
  ]

  include_dirs += [
    "//foundation/multimedia/image_framework/plugins/manager/include/pluginbase",
    "//foundation/multimedia/image_framework/plugins/common/libs/image/libjpegplugin/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/include",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/include",
    "$skia_root/skia",
    "$skia_root/skia/include/core",
    "$skia_root/skia/include/codec",
    "$skia_root/skia/third_party/externals/libjpeg-turbo",
    "$skia_root/skia/third_party/skcms",
    "//third_party/libexif",
  ]

  if (use_mingw_win) {
    include_dirs +=
        [ "//foundation/multimedia/image_framework/mock/native/include" ]
  } else if (use_clang_mac) {
    include_dirs += [
      "//foundation/multimedia/image_framework/mock/native/include",
      "//third_party/bounds_checking_function/include",
      "//commonlibrary/c_utils/base/include",
      "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    ]
  } else {
    include_dirs += [
      "//commonlibrary/c_utils/base/include",
      "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    ]
  }
}

js_declaration("image_js") {
  part_name = "image_framework"
  sources = [ "//foundation/multimedia/image_framework/interfaces/kits/js/@ohos.multimedia.image.d.ts" ]
}

ohos_copy("image_declaration") {
  sources = [ "//foundation/multimedia/image_framework/interfaces/kits/js/@ohos.multimedia.image.d.ts" ]
  outputs = [ target_out_dir + "/$target_name/" ]
  module_source_dir = target_out_dir + "/$target_name"
  module_install_name = ""
}

config("image_postproc_config") {
  include_dirs = [ "//third_party/ffmpeg" ]
}

if (use_clang_android) {
  ohos_source_set("image_native") {
    public_configs = [ ":image_external_config" ]
    configs = [ ":image_postproc_config" ]
    defines = image_decode_android_defines
    cflags = image_native_android_cflags
    sources = image_native_android_sources
    include_dirs = image_native_android_include_dirs
    deps = image_native_android_deps
    deps += [ "//third_party/ffmpeg:libohosffmpeg_static" ]
    subsystem_name = "multimedia"
    part_name = "image_framework"
  }
} else if (use_clang_ios) {
  ohos_source_set("image_native") {
    public_configs = [ ":image_external_config" ]
    configs = [ ":image_postproc_config" ]
    defines = image_decode_ios_defines
    cflags = image_native_ios_cflags
    include_dirs = image_native_ios_include_dirs
    sources = image_native_ios_sources
    deps = image_native_ios_deps
    deps += [ "//third_party/ffmpeg:libohosffmpeg_static" ]
    external_deps = image_native_ios_external_deps
    subsystem_name = "multimedia"
    part_name = "image_framework"
  }
} else {
  ohos_shared_library("image_native") {
    if (!use_clang_android) {
      sanitize = {
        cfi = true
        cfi_cross_dso = true
        debug = false
      }
    }
    public_configs = [ ":image_external_config" ]
    configs = [ ":image_postproc_config" ]

    cflags = [
      "-DIMAGE_DEBUG_FLAG",
      "-DIMAGE_COLORSPACE_FLAG",
    ]

    sources = [
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/abstract_exif_metadata_accessor.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/buffer_metadata_stream.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/data_buf.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/exif_metadata.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/exif_metadata_formatter.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/file_metadata_stream.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/jpeg_exif_metadata_accessor.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/metadata_accessor_factory.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/png_exif_metadata_accessor.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/png_image_chunk_utils.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/tiff_parser.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/webp_exif_metadata_accessor.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/common/src/memory_manager.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/common/src/native_image.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/common/src/pixel_astc.cpp",
      "${image_subsystem}/frameworks/innerkitsimpl/common/src/pixel_yuv.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_source.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/incremental_pixel_map.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/pixel_map.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/pixel_map_parcel.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/basic_transformer.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/matrix.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/pixel_convert.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/post_proc.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/scan_line_filter.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/image_format_convert_utils.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/image_format_convert.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/image_utils_tools.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator_manager.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver_manager.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_source_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_source_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/incremental_source_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/istream_source_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
    ]

    if (use_mingw_win) {
      defines = image_decode_windows_defines
      sources -= [
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
      ]
      deps = [
        "//foundation/graphic/graphic_2d/utils/color_manager:color_manager",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils_static",
        "//foundation/multimedia/image_framework/mock/native:log_mock_static",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
        "//third_party/ffmpeg:libohosffmpeg_static",
      ]
      external_deps = [ "graphic_surface:surface" ]
    } else if (use_clang_mac) {
      defines = image_decode_mac_defines
      sources -= [
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
      ]
      deps = [
        "//foundation/graphic/graphic_2d/utils/color_manager:color_manager",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils_static",
        "//foundation/multimedia/image_framework/mock/native:log_mock_static",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
        "//third_party/bounds_checking_function:libsec_statics",
        "//third_party/ffmpeg:libohosffmpeg_static",
      ]
      external_deps = [ "graphic_surface:surface" ]
    } else {
      defines = [ "DUAL_ADAPTER" ]
      DUAL_ADAPTER = true

      deps = [
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter",
        "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
        "//third_party/ffmpeg:libohosffmpeg",
      ]
      deps += skia_platform

      if (DUAL_ADAPTER) {
      } else {
        deps += [ "//third_party/libpng:libpng" ]
      }
      external_deps = [
        "c_utils:utils",
        "graphic_2d:color_manager",
        "graphic_surface:surface",
        "hilog:libhilog",
        "hitrace:hitrace_meter",
        "ipc:ipc_core",
        "napi:ace_napi",
        "zlib:libz",
      ]
      if (enable_libexif) {
        external_deps += [ "libexif:libexif" ]
      }
    }

    if (purgeable_ashmem_enable && defined(global_parts_info) &&
        defined(global_parts_info.resourceschedule_memmgr_plugin)) {
      defines += [ "IMAGE_PURGEABLE_PIXELMAP" ]
      external_deps += [ "memmgr_plugin:libpurgeablemem_plugin" ]
    }
    external_deps += [ "zlib:libz" ]

    if (defined(global_parts_info) &&
        defined(global_parts_info.graphic_graphic_2d_ext)) {
      defines += [ "SUT_DECODE_ENABLE" ]
    }

    if (defined(global_parts_info) &&
        defined(global_parts_info.open_source_libyuv)) {
      defines += [ "LIBYUV_ENABLE" ]
      external_deps += [ "libyuv:yuv" ]
    }

    #  relative_install_dir = "module/multimedia"
    subsystem_name = "multimedia"
    innerapi_tags = [ "platformsdk" ]
    part_name = "image_framework"
    version_script = "libimage_native.versionscript"
  }
}

ohos_static_library("image_static") {
  public_configs = [ ":image_external_config" ]

  sources = [
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/abstract_exif_metadata_accessor.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/buffer_metadata_stream.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/data_buf.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/exif_metadata.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/exif_metadata_formatter.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/file_metadata_stream.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/jpeg_exif_metadata_accessor.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/metadata_accessor_factory.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/png_exif_metadata_accessor.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/png_image_chunk_utils.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/tiff_parser.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/accessor/src/webp_exif_metadata_accessor.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/common/src/pixel_astc.cpp",
    "${image_subsystem}/frameworks/innerkitsimpl/common/src/pixel_yuv.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_source.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/incremental_pixel_map.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/pixel_map.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/common/src/image_utils_tools.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/basic_transformer.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/matrix.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/pixel_convert.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/post_proc.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/scan_line_filter.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/image_format_convert_utils.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/converter/src/image_format_convert.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator_manager.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver_manager.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_source_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_source_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/incremental_source_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/istream_source_stream.cpp",
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
  ]

  if (use_mingw_win) {
    defines = image_decode_windows_defines
    sources -= [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
    ]
    deps = [
      "//foundation/graphic/graphic_2d/utils/color_manager:color_manager",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils_static",
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
    ]
    external_deps = [ "graphic_surface:surface" ]
  } else if (use_clang_mac) {
    defines = image_decode_mac_defines
    sources -= [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/codec/src/image_packer_ex.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/buffer_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/file_packer_stream.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/stream/src/ostream_packer_stream.cpp",
    ]

    deps = [
      "//foundation/graphic/graphic_2d/utils/color_manager:color_manager",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils_static",
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
      "//third_party/bounds_checking_function:libsec_static",
    ]
    external_deps = [ "graphic_surface:surface" ]
  } else if (use_clang_ios) {
    defines = image_decode_ios_defines
    sources -= [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator_manager.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver_manager.cpp",
    ]
    deps = [
      "$image_subsystem/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
      "//foundation/arkui/napi:ace_napi",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils",
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
    ]
  } else if (use_clang_android) {
    defines = image_decode_android_defines
    sources -= [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/creator/src/image_creator_manager.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/receiver/src/image_receiver_manager.cpp",
    ]
    deps = [
      "$image_subsystem/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
      "//commonlibrary/c_utils/base:utils",
      "//foundation/arkui/napi:ace_napi",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
    ]
  } else {
    defines = [ "DUAL_ADAPTER" ]
    DUAL_ADAPTER = true

    deps = [
      "$image_subsystem/frameworks/innerkitsimpl/pixelconverter:pixelconvertadapter_static",
      "//foundation/arkui/napi:ace_napi",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils:image_utils",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
    ]

    if (DUAL_ADAPTER) {
    } else {
      deps += [ "//third_party/libpng:libpng" ]
    }
    external_deps = [
      "c_utils:utils",
      "graphic_2d:color_manager",
      "graphic_surface:surface",
      "hilog:libhilog",
    ]
  }
  external_deps += [ "zlib:libz" ]

  if (defined(global_parts_info) &&
      defined(global_parts_info.open_source_libyuv)) {
    defines += [ "LIBYUV_ENABLE" ]
    cflags += [ "-DCAMERA_MMAP_RESERVE" ]
    external_deps += [ "libyuv:yuv" ]
  }

  subsystem_name = "multimedia"
  part_name = "image_framework"
}
